package com.demoapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demoapp.dao.UserDao;
import com.demoapp.model.User;

@Service
public class UserService {

	@Autowired
	UserDao userDao;
	
	public User getUser(String name) {
		return userDao.findById(name).orElse(null);
	}
	
	public List<User> getUsers() {
		return userDao.findAll();
	}
	
	public User addUser(User user) {
		return userDao.save(user);
	}
	
	public void updateUser(User user) {
		User savedUser = getUser(user.getName());
		savedUser.setAge(user.getAge());
		userDao.save(savedUser);
	}
	
	public void deleteUser(User user) {
		userDao.delete(user);
	}
}
