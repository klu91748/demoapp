package com.demoapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demoapp.model.User;

@Repository
public interface UserDao extends JpaRepository<User, String>{

}
