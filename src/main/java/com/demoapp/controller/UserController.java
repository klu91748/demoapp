package com.demoapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demoapp.exception.UserAlreadyExistException;
import com.demoapp.exception.UserNotFoundException;
import com.demoapp.model.User;
import com.demoapp.service.UserService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {
	
	@Autowired
	UserService userService;
	
	private Logger logger = LoggerFactory.getLogger(UserController.class);

	@GetMapping("/users/{name}")
	public ResponseEntity<User> getUser(@PathVariable String name) {
		User user = userService.getUser(name);
		if (user == null) {
			logger.warn("Throwing UserNotFoundException");
			throw new UserNotFoundException();
		}
		logger.info("Retrieving User " + name);
		return new ResponseEntity<>(user, HttpStatus.OK);	
	}
	
	@PostMapping("/users")
	public ResponseEntity<User> addUser(@RequestBody User user) {
		if (userService.getUser(user.getName()) != null) {
			logger.warn("Throwing UserAlreadyExistException");
			throw new UserAlreadyExistException();
		}	
		userService.addUser(user);
		logger.info("Created User " + user.getName());
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}
	
	@PutMapping("/users")
	public ResponseEntity<User> updateUser(@RequestBody User user) {
		if (userService.getUser(user.getName()) == null) {
			logger.warn("Throwing UserNotFoundException");
			throw new UserNotFoundException();
		}
		userService.updateUser(user);
		logger.info("Updated User " + user.getName());
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
	
	@DeleteMapping("/users/{name}")
	public ResponseEntity<Object> deleteUser(@PathVariable String name) {
		User user = userService.getUser(name);
		if (user == null) {
			logger.warn("Throwing UserNotFoundException");
			throw new UserNotFoundException();
		}
		userService.deleteUser(user);
		logger.info("Deleted User " + name);
		return new ResponseEntity<>("Successfully deleted " + name + "!", HttpStatus.OK);
	}
}
