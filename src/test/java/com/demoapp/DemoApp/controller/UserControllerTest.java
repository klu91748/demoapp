package com.demoapp.DemoApp.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.demoapp.dao.UserDao;
import com.demoapp.model.User;
import com.demoapp.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

	@Autowired
	private UserService userService;
	
	@MockBean
	private UserDao userDao;
	
	@Test
	public void getUserTest() {
		userService.getUser("kevin");
		verify(userDao, times(1)).findById("kevin");
	}
	
	@Test
	public void getUsersTest() {
		when(userDao.findAll()).thenReturn(Stream
				.of(new User("kevin", 25)).collect(Collectors.toList()));
		assertEquals(1, userService.getUsers().size());
	}
	
	@Test
	public void updateUserTest() {
		User user = new User("kevin", 23);
		when(userDao.save(user)).thenReturn(user);
		assertEquals(user, userService.addUser(user));
	}
	
	@Test
	public void saveUserTest() {
		User user = new User("kevin", 23);
		when(userDao.save(user)).thenReturn(user);
		assertEquals(user, userService.addUser(user));
	}
	
	@Test
	public void deleteUserTest() {
		User user = new User("kevin", 23);
		userService.deleteUser(user);
		verify(userDao, times(1)).delete(user);
	}
	
}
